#!/bin/python3

import os
from rethinkdb import RethinkDB
from rethinkdb.errors import RqlRuntimeError, RqlDriverError

r = RethinkDB()

db_host = 'rethinkdb.demo-changefeeds.svc.cluster.local'
db_port = '28015'
spectare_db = 'demo-changefeeds'
table_list = ['demo1','demo2']
def db_create():
    connection = r.connect(host=db_host, port=db_port)
    try:
        r.db_create(spectare_db).run(connection)
        print("Database setup completed.")
    except RqlRuntimeError:
        print("Database already exists.")
    finally:
        connection.close()

def db_setup(table_list):
    connection = r.connect(host=db_host, port=db_port)
    try:
        for table in table_list:
            try:
                r.db(spectare_db).table_create(table).run(connection)
                print("Created table: " + table)
            except RqlRuntimeError:
                print("Table already exists: " + table)
    finally:
        connection.close()

db_create()
db_setup(table_list)
exit(0)